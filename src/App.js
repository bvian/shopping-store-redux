import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';

import './App.css';
import Navbar from './components/Navbar';
import Loader from './components/layout/Loader';
import Error from './components/layout/Error';
import Products from './components/Products';
import NoProductsFound from './components/layout/NoProductsFound';
import AddNewProduct from './components/AddNewProduct';
import UpdateProduct from './components/UpdateProduct';
import CartScreen from './Pages/CartScreen';
import Footer from './components/Footer';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      status: '',
      errorMessage: "",
    };

    this.URL = 'https://fakestoreapi.com/products';
  }

  fetchData = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      axios.get(url)
        .then((response) => {
          this.props.initialProducts(response.data);
          this.setState({
            status: this.API_STATES.LOADED
          });
        })
        .catch((err) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occurred. Please try again in a few minutes."
          });
        });
    });
  }

  componentDidMount() {
    this.fetchData(this.URL);
  }

  render() {

    return (
      <Router>
        <div className='App'>
          <Navbar />

          {this.state.status === this.API_STATES.LOADING && <Loader />}
          {this.state.status === this.API_STATES.ERROR && <Error errorMessage={this.state.errorMessage} />}
          {this.state.status === this.API_STATES.LOADED && this.props.products.length === 0 && <NoProductsFound head='No Products Found' message='No products available at the moment. Please try again later.' />}
          {
            this.state.status === this.API_STATES.LOADED && this.props.products.length > 0 &&
            <Routes>
              <Route path="/" element={<Products products={this.props.products} />} />
              <Route path="/add-product" element={<AddNewProduct />} />
              <Route path="/update-product/:id" element={<UpdateProduct products={this.props.products} />} />
              <Route path="/cart" element={<CartScreen />} />
              <Route path="*" element={<NoProductsFound head='Server Error' message='Unfortunately, the page you are looking for does not exist.' />} />
            </Routes>
          }

          <Footer />
        </div>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {

  const products = state.products.list;

  return {
    products
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    initialProducts: (products) => dispatch({
      type: 'INITIAL_PRODUCTS',
      payload: products
    })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

