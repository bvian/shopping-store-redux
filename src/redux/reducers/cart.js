import { ADD_PRODUCT_TO_CART, DECREASE_QUANTITY, DELETE_PRODUCT_FROM_CART, INCREASE_QUANTITY } from "../actionTypes";

const initialState = {
    list: []
}

export default function actionProduct(state = initialState, action) {
    switch (action.type) {
        case INCREASE_QUANTITY: {
            return {
                ...state,
                list: state.list.map((currentProduct) => {
                    if (currentProduct.id === action.payload) {
                        const modifiedProduct = {
                            ...currentProduct,
                            quantity: currentProduct.quantity + 1
                        };
                        return modifiedProduct;
                    } else {
                        return currentProduct;
                    }
                })
            }
        }

        case DECREASE_QUANTITY: {
            return {
                ...state,
                list: state.list.map((currentProduct) => {
                    if (currentProduct.id === action.payload) {
                        const modifiedProduct = {
                            ...currentProduct,
                            quantity: currentProduct.quantity - 1
                        };
                        return modifiedProduct;
                    } else {
                        return currentProduct;
                    }
                })
            }
        }

        case ADD_PRODUCT_TO_CART: {
            return {
                ...state,
                list: [
                    ...state.list,
                    action.payload
                ]
            }
        }

        case DELETE_PRODUCT_FROM_CART: {
            return {
                ...state,
                list: state.list
                    .filter((currentProduct) => {
                        return currentProduct.id !== action.payload;
                    })
            }
        }

        default: {
            return state;
        }
    }
}