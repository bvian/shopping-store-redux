import { ADD_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT, INITIAL_PRODUCTS } from "../actionTypes";

const initialState = {
    list: [],
    // screen: 'home'
}

export default function actionProduct(state = initialState, action) {
    switch (action.type) {

        case INITIAL_PRODUCTS: {
            return {
                ...state,
                list: action.payload
            }
        }

        case ADD_PRODUCT: {
            return {
                ...state,
                list: [
                    ...state.list,
                    action.payload
                ]
            }
        }

        case EDIT_PRODUCT: {
            return {
                ...state,
                list: state.list.map((currentProduct) => {
                    return currentProduct.id === action.payload.id ? action.payload : currentProduct;
                })
            }
        }

        case DELETE_PRODUCT: {
            return {
                ...state,
                list: state.list
                    .filter((currentProduct) => {
                        return currentProduct.id !== action.payload;
                    })
            }
        }

        default: {
            return state;
        }
    }
}