import { configureStore } from '@reduxjs/toolkit';

import products from './reducers/products';
import cart from './reducers/cart';

const reducer = {
    products,
    cart
};

export default configureStore({
    reducer
});
