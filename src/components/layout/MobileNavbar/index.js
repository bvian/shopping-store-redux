import React from 'react';

import './MobileNavbar.css';

class MobileNavbar extends React.Component {

    handleClick = () => {
        this.props.showMobileNavbar();
    }

    render() {
        return (
            <div id="mobile" onClick={this.handleClick}>
                <i id="bar" className="fas fa-outdent"></i>
            </div>
        );
    }
}

export default MobileNavbar;