import React from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './NavMiddle.css';

class NavMiddle extends React.Component {

    render() {
        return (
            <div className="navMiddle">
                <ul id="navbar" className={!this.props.mobileNav ? 'hide' : ''}>
                    <li><Link to="/" className="active nav-ele">Home</Link></li>
                    <li><Link to="/" className="nav-ele">Shop</Link></li>
                    <li><Link to="/" className="nav-ele">About</Link></li>
                    <li><Link to="/" className="nav-ele">Contact</Link></li>
                    <li><Link to="/cart" className="nav-ele">
                        <i className="far fa-shopping-bag">
                            <div>
                                {this.props.totalCartProducts > 0 &&
                                    <div className='cart-count'>{this.props.totalCartProducts}</div>
                                }
                            </div>
                        </i></Link></li>
                    <ul className="navRightContainer" id="mobileRight">
                        <li style={{ fontSize: '1em', width: '12em', padding: '0' }}>
                            <Link to="/add-product" id="addNewProduct">ADD NEW PRODUCT</Link>
                        </li>
                    </ul>
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const totalCartProducts = state.cart.list.length;
    return {
        totalCartProducts
    };
}

export default connect(mapStateToProps)(NavMiddle);