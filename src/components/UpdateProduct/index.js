import React from 'react';

import ProductForm from '../ProductForm';

class UpdateProduct extends React.Component {

    id = window.location.href.split('/')
        .slice(4)
        .join('');

    getProduct = () => {
        const product = this.props.products.find((currentProduct) => {
            return currentProduct.id === (isNaN(Number(this.id)) ? this.id : Number(this.id));
        });

        return product;
    }


    render() {
        return (
            <ProductForm headTitle="UPDATE PRODUCT" product={this.getProduct()} />
        );
    }
}

export default UpdateProduct;