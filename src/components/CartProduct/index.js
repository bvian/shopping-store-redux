import React from 'react';
import { connect } from 'react-redux';

import './CartProduct.css';

class CartProduct extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            popUpScreen: false,
        }
    }

    yesButton = () => {
        this.props.deleteProductFromCart(this.props.cartProduct.id);
        this.setState({
            popUpScreen: !this.state.popUpScreen,
        });
        setTimeout(() => {
            this.props.getTotalAmount();
        },100);
    }

    noButton = () => {
        this.setState({
            popUpScreen: !this.state.popUpScreen,
        });
    }

    increaseItem = () => {
        this.props.increaseQuantity(this.props.cartProduct.id);
    }

    decreaseItem = () => {
        if (this.props.cartProduct.quantity > 1) {
            this.props.decreaseQuantity(this.props.cartProduct.id);
        } else {
            this.setState({
                popUpScreen: !this.state.popUpScreen,
            });
        }
    }

    componentDidUpdate() {
        this.props.getTotalAmount();
    }

    render() {
        const {
            title,
            image,
            price,
            quantity
        } = this.props.cartProduct;

        return (
            <div className="cart-details-container" >
                {this.state.popUpScreen &&
                    <div id="popup-screen">
                        <span>Are you sure you want to remove the item from your cart?</span>
                        <div className="popup-screen-buttons">
                            <button onClick={this.yesButton}>Yes</button>
                            <button onClick={this.noButton}>No</button>
                        </div>
                    </div>
                }
                {
                    <>
                        <div className='cart-item-details'>
                            <div className='cart-item-image-container'>
                                <img
                                    alt=""
                                    src={image}
                                />
                            </div>
                            <div className='cart-single-product-title'>{title}</div>
                        </div>
                        <span className='cart-item-price'>${price}</span>
                        <span className='cart-item-quantity'>
                            <span className='minus' onClick={this.decreaseItem}>-</span>
                            <span className='item-quantity'>{quantity}</span>
                            <span className='plus' onClick={this.increaseItem}>+</span>
                        </span>
                        <span className='cart-item-subtotal'>${(quantity * price).toFixed(2)}</span>
                    </>
                }
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        increaseQuantity: (productId) => dispatch({
            type: 'INCREASE_QUANTITY',
            payload: productId
        }),

        decreaseQuantity: (productId) => dispatch({
            type: 'DECREASE_QUANTITY',
            payload: productId
        }),

        deleteProductFromCart: (productId) => dispatch({
            type: 'DELETE_PRODUCT_FROM_CART',
            payload: productId
        })
    }
}

export default connect(null, mapDispatchToProps)(CartProduct);